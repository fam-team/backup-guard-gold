<?php

require_once(SG_LIB_PATH.'SGReplace.php');

class SGMigrate
{
	private $sgReplace = null;
	private $dbAdapter = null;

	public function __construct($dbAdapter=null)
	{
		$this->dbAdapter = $dbAdapter;
		$this->sgReplace = new SGReplace($this);
	}

	public function get_rows( $table )
	{
		$table = esc_sql( $table );

		$count = 0;
		$tableRowsNum = $this->dbAdapter->query('SELECT COUNT(*) AS total FROM '.$table);
		$count = @$tableRowsNum[0]['total'];
		return $count;
	}

	public function get_columns( $table )
	{
		$primary_key = NULL;
		$columns = array();
		$fields = $this->dbAdapter->query( 'DESCRIBE ' . $table, array(), OBJECT);

		if ( is_array( $fields ) ) {
			foreach ( $fields as $column ) {
				$columns[] = $column->Field;
				if ( 'PRI' === $column->Key ) {
					$primary_key = $column->Field;
				}
			}
		}

		return array( $primary_key, $columns );
	}

	public function get_table_content( $table, $start, $end )
	{
		$data = $this->dbAdapter->query( "SELECT * FROM $table LIMIT $start, $end");

		return $data;
	}

	public function update( $table, $update_sql, $where_sql )
	{
		$sql = 'UPDATE ' . $table . ' SET ' . implode( ', ', $update_sql ) .
			   ' WHERE ' . implode( ' AND ', array_filter( $where_sql ) );

		return $this->dbAdapter->exec( $sql );
	}

	public function flush()
	{
		$this->dbAdapter->flush();
	}

	public function migrateMultisite($oldValue, $newValue, $tables)
	{
		return $this->sgReplace->run_search_replace($oldValue, $newValue, $tables);
	}

	public function migrate($oldValue, $newValue, $tables)
	{
		return $this->sgReplace->run_search_replace($oldValue, $newValue, $tables);
	}

	public function replaceValuesInQuery($oldValue, $newValue, $query)
	{
		return $this->sgReplace->replaceValuesInQuery($oldValue, $newValue, $query);
	}
}
