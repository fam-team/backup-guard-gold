<?php

/**
 * Plugin Name:       BackupGuard Pro
 * Plugin URI:        https://backup-guard.com/products/backup-wordpress
 * Description:       BackupGuard Pro for WordPress is the best backup choice for WordPress based websites or blogs.
 * Version:           1.1.34
 * Author:            BackupGuard
 * Author URI:        https://backup-guard.com/products/backup-wordpress
 * License:           Commercial Software License
 * License URI:
 */

if (function_exists('activate_backup_guard')) {
	die('Please deactivate any other BackupGuard version before activating this one.');
}

define('SG_BACKUP_GUARD_VERSION', '1.1.34');

// If this file is called directly, abort.
if (!defined('WPINC')) {
	die;
}

require_once(plugin_dir_path(__FILE__).'public/boot.php');
require_once(plugin_dir_path(__FILE__).'BackupGuard.php');
